const axios = require("axios");

function uniqueArrayOfObject(array, keyToBeUnique) {
  return Object.values(array.reduce((tmp, x) => {
    if (tmp[x[keyToBeUnique]]) return tmp;
    tmp[x[keyToBeUnique]] = x;
    return tmp;
  }, {}));
}


function GetProducts(done = [], start = 0, max = 100000){
    return new Promise(async (resolve, reject) => {
        let k = await axios.get(`https://store.steampowered.com/search/results/?query&start=${start}&count=${Math.min(50, max-start)}&specials=1&infinite=1`)
        max = Math.min(max, k.data.total_count)
        let j = k.data.results_html.split(`<a href=\"https://store.steampowered.com/`)
        j.shift();
        j = j.map(el => {
            return {
            id: el.split("/")[0]+":"+el.split("/")[1],
            url:`https://store.steampowered.com/${el.split("\"")[0]}`,
            img:el.split(" 2x")[0].split(" ").pop(),
            tags: el.split('data-ds-tagids="').length>1?JSON.parse(el.split('data-ds-tagids="')[1].split("\"")[0]):[],
            curators:el.split('data-ds-crtrids="').length>1?JSON.parse(el.split('data-ds-crtrids="')[1].split("\"")[0]):[],
            title: el.split('<span class="title">')[1].split("</span>")[0],
            platforms: el.split("<p>")[1].split("</p>")[0].split("platform_img ").map(el => el.replace('<span class="',"").replace('"></span>',"").replace("\r\n","").replace(/\s/g,"")).filter(el => el!=""),
            released: el.split('<div class="col search_released responsive_secondrow">')[1].split("</div>")[0],
            review: el.split('search_review_summary').length>1?el.split('<span class="search_review_summary')[1].split('&')[0].split('data-tooltip-html="')[1]:"N/A",
            price_data: Number(el.split('data-price-final="')[1].split("\"")[0]),
            discount: el.split('<div class="col search_discount responsive_secondrow">')[1].split("<span>").length>1?el.split('<div class="col search_discount responsive_secondrow">')[1].split('<span>')[1].split('</span>')[0]:"N/A",
            old_price: el.split("<strike>").length>1?el.split("<strike>")[1].split('</strike>')[0]:"N/A",
            new_price: el.split("<strike>").length>1?el.split("</strike></span><br>")[1].split(" ")[0]:"N/A",
            }
        })

        done = uniqueArrayOfObject(done.concat(j), "url").slice(0, max);
        if(done.length != max && max-start >=0)
        {
            resolve(GetProducts(done, start+50, max))
        }
        else
        {
            resolve(done)
        }
    })
}

module.exports = (max) => GetProducts([], 0, max)

