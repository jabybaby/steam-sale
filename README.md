# Introduction
This module is a promise-based scraper of Steam sales using the infinite scrolling web API endpoint. I didn't get rate limited while trying to scrape every deal. It took a minute, but the promise finally resolved into the correct array.
The imported module is a single function which accepts one parameter and returns a promise. The parameter is optional and can be used to set the maximum amount of deals returned by the function. If it's left as undefined, the scraper will return every deal. Of course, the smaller number you set the quicker you'll get your result.
The only dependency of this module has is axios. No HTML parsing, just pure text manipulation.

This is my second package, please don't judge me. Happy scraping everyone!

# Installation

    npm i steam-sale

# Example usage

    const Steam = require("steam-sale")
    async function Test()
    {
    	const SaleItems = await Steam(10);
    	console.log(SaleItems)
    }
    
    Test()

# Output

      [{
        id: 'app:287700', //'app' or 'bundle' and the ID of the item separated by a colon.
        url: 'https://store.steampowered.com/app/287700/METAL_GEAR_SOLID_V_THE_PHANTOM_PAIN/?snr=1_7_7_2300_150_1', // The url of the item
        img: 'https://cdn.akamai.steamstatic.com/steam/apps/287700/capsule_231x87.jpg?t=1591740509', // 2x sized image url (shown while hovering over the item)
        tags: [
          1687, 1695,   19,
          1742, 1708, 4145,
          1756
        ], // IDs of the tags attached to the item (You could check the meaning of the ID at https://steamdb.info/tag/<id> or https://store.steampowered.com/search/?tags=<id1>,<id2>,-<excludedId>)
        curators: [ 39026134 ], // IDs of the curators associated with the game (check https://store.steampowered.com/curator/<id>)
        title: 'METAL GEAR SOLID V: THE PHANTOM PAIN', // Name of the item
        platforms: [ 'win' ], // Platforms the game can be played on 
        released: '1 Sep, 2015', // Release date
        review: 'Very Positive', // Review summary
        price_data: 749, // Price as a number, in my region it's the number of cents it costs, divide by 100 to get the actual price in the region's currency
        discount: '-75%', // Discount percentage OR 'N/A'
        old_price: '29,99€', // Original price or 'N/A'
        new_price: '7,49€' // Discounted price or 'N/A'
      }
      ...
      ]

# N/A and other values that don't make sense
Steam lists some games that cannot be bought separately, only in a bundle. These consequently don't have a price tag either. They are included because if the bundle is on sale the game itself gets listed (even if you can't buy the game itself). Keep them or remove them, these are included in the maximum amount you set and I've decided to return them for clarity's sake.
